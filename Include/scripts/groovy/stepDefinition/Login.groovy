package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.ElementNotSelectableException

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable

public class Login {
	@Given("User go to login page")
	public void user_go_to_login_page() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://magento.softwaretestingboard.com/')
		WebUI.verifyElementVisible(findTestObject('Landing_page/txt_New_Luma_Yoga_Collection'))
		WebUI.click(findTestObject('Landing_page/btn_Masuk'))
		WebUI.verifyElementVisible(findTestObject('Login_page/txt_Customer_Login'))
	}

	@When("User input email field with (.*) email")
	public void user_input_email_field_with_email(String email){
		if(email=='correct') {
			WebUI.setText(findTestObject('Login_page/inp_Email'), 'xiximo7081@favilu.com')
		}else if(email=='incorrect format') {
			WebUI.setText(findTestObject('Login_page/inp_Email'), 'xiximo7081favilu.com')
		}else if(email=='incorrect') {
			WebUI.setText(findTestObject('Login_page/inp_Email'), 'andaikutau@mail.com')
			
		}else if(email=='empty') {
			WebUI.setText(findTestObject('Login_page/inp_Email'), '')
		}
	}

	@When("User input password field with (.*) password")
	public void user_input_password_field_with_password(String password) {
		if(password=='correct') {
			WebUI.setText(findTestObject('Login_page/inp_Password'), 'A123456789$')
		}else if(password=='incorrect') {
			WebUI.setText(findTestObject('Login_page/inp_Password'), '123456789')
		}else if(password=='empty') {
			WebUI.setText(findTestObject('Login_page/inp_Password'), '')
		}
	}

	@When("User click Masuk button")
	public void user_click_Masuk_button() {
		WebUI.click(findTestObject('Login_page/btn_Masuk'))
	}

	@Then("User (.*) login")
	public void user_login(String result) {
		if(result=='successfully') {
			WebUI.verifyElementVisible(findTestObject('Home_page/txt_Welcome_User'))
		}else if(result=='failed') {
			WebUI.verifyElementVisible(findTestObject('Login_page/txt_Customer_Login'))
		}
	}
}