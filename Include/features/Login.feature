@Login
Feature: Login

  Scenario Outline: User want to login with <condition>
    Given User go to login page
    When User input email field with <email> email
    And User input password field with <password> password
    And User click Masuk button
    Then User <result> login

    Examples: 
      | case     | condition            | email            | password  | result       |
      | positive | valid credential     | correct          | correct   | successfully |
      | negative | wrong password       | correct          | incorrect | failed       |
      | negative | invalid email format | incorrect format | correct   | failed       |
      | negative | not registered email | incorrect        | correct   | failed       |
      | negative | empty email field    | empty            | correct   | failed       |
      | negative | empty password field | correct          | empty     | failed       |
