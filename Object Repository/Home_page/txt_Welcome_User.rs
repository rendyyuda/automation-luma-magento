<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_Welcome_User</name>
   <tag></tag>
   <elementGuidId>212099c3-e1f0-4a45-8766-ff09e83aced2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to Content'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.logged-in</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d6540535-3f12-438e-8f4f-3bc2498073b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>logged-in</value>
      <webElementGuid>0a234a4b-8ae2-4723-b2b6-dfc847125db9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>text: new String('Welcome, %1!').replace('%1', customer().fullname)</value>
      <webElementGuid>645fd72e-4891-48e4-876f-fa4303c11990</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Welcome, Ucok Saputra!</value>
      <webElementGuid>03c319bb-c434-452c-96a2-a6b5696cbdbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column&quot;]/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;greet welcome&quot;]/span[@class=&quot;logged-in&quot;]</value>
      <webElementGuid>a6172c3f-d238-492f-9972-16c7e56916c3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to Content'])[1]/following::span[1]</value>
      <webElementGuid>5b23bf07-5bd1-406a-afe7-7648f637f3bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change'])[1]/preceding::span[1]</value>
      <webElementGuid>873b6723-6b6f-47c0-8863-5b50af194689</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Account'])[1]/preceding::span[3]</value>
      <webElementGuid>7fb4bdb8-8ba0-43fc-b9dd-a42466d0b397</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Welcome, Ucok Saputra!']/parent::*</value>
      <webElementGuid>bbac7176-c62a-4a2e-86dd-b93f8d80a1d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/span</value>
      <webElementGuid>36c2605d-be41-4cc2-ae5d-8a03825fdf05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Welcome, Ucok Saputra!' or . = 'Welcome, Ucok Saputra!')]</value>
      <webElementGuid>a49ddfa5-98e3-4164-a90b-fcaa36a79f73</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
