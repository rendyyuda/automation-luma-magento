<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_Error_Message</name>
   <tag></tag>
   <elementGuidId>d6424bef-3de1-4a9f-91a7-6a00ff307ef9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div[2]/div[2]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.message-error.error.message</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2bf899c7-d137-4069-8a16-14dc2559e415</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message',
            'data-ui-id': 'message-' + message.type
        }</value>
      <webElementGuid>f2a2ca2a-8064-48e5-873f-59983aab068d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>message-error error message</value>
      <webElementGuid>6dcd131b-9918-40e8-b047-2c1b11be4c37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ui-id</name>
      <type>Main</type>
      <value>message-error</value>
      <webElementGuid>17354f74-0622-4916-b5be-3bd9517177dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
The account sign-in was incorrect or your account is disabled temporarily. Please wait and try again later.
</value>
      <webElementGuid>c7637046-673c-46e4-a048-030e07385aaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;page messages&quot;]/div[2]/div[@class=&quot;messages&quot;]/div[@class=&quot;message-error error message&quot;]</value>
      <webElementGuid>2b0f9875-04f4-4a8a-b6c1-235b28b782c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div[2]/div[2]/div/div</value>
      <webElementGuid>5118a6e7-61ae-4511-bc61-20a00dbbcda0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Login'])[2]/following::div[5]</value>
      <webElementGuid>de549d7c-c007-45f8-89ed-df64c857d7b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an Account'])[2]/following::div[6]</value>
      <webElementGuid>ed0d0602-bf70-4cb6-a1f3-f2bb29135644</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Registered Customers'])[1]/preceding::div[3]</value>
      <webElementGuid>992446b7-1b52-4360-adfb-007d88718bb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you have an account, sign in with your email address.'])[1]/preceding::div[4]</value>
      <webElementGuid>c85facef-21f4-406c-bde6-690484864587</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div</value>
      <webElementGuid>d4da959c-9b2a-4140-8af9-ad35e2ac1dba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
The account sign-in was incorrect or your account is disabled temporarily. Please wait and try again later.
' or . = '
The account sign-in was incorrect or your account is disabled temporarily. Please wait and try again later.
')]</value>
      <webElementGuid>89e595d3-c507-4705-8efe-0919c8741df4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
